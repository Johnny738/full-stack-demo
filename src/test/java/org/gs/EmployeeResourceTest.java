package org.gs;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.hasItems;

import javax.json.Json;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
@Tag("integration")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class EmployeeResourceTest {

    // TEST 1
    @Test
    @Order(1)
    void testGetAll() {
	given().when().get("/employees").then().body("size()", equalTo(2))
		.body("id", hasItems(1, 2)).body("first_name", hasItems("Daniel", "Karl"))
		.body("last_name", hasItems("Johnsson", "Karlson"))
		.body("position", hasItems("Taxi Driver", "Developer"))
		.body("country", hasItem("Austria")).statusCode(Response.Status.OK.getStatusCode());
    }

    // TEST 2
    @Test
    @Order(1)
    void testGetById() {
	given().when().get("/employees/1").then().body("id", equalTo(1))
		.body("first_name", equalTo("Daniel")).body("last_name", equalTo("Johnsson"))
		.body("position", equalTo("Taxi Driver")).body("country", equalTo("Austria"))
		.statusCode(Response.Status.OK.getStatusCode());
    }

    @Test
    @Order(1)
    void testGetByIdKO() {
	given().when().get("/employees/500").then()
		.statusCode(Response.Status.NOT_FOUND.getStatusCode());
    }

    // TEST 3
    @Test
    @Order(1)
    void testGetByFirtsName() {
	given().when().get("/employees/first_name/Daniel").then().body("id", equalTo(1))
		.body("first_name", equalTo("Daniel")).body("last_name", equalTo("Johnsson"))
		.body("position", equalTo("Taxi Driver")).body("country", equalTo("Austria"))
		.statusCode(Response.Status.OK.getStatusCode());
    }

    @Test
    @Order(1)
    void testGetByFirstNameKO() {
	given().get("/employees/first_name/JohnDow").then()
		.statusCode(Response.Status.NOT_FOUND.getStatusCode());
    }

    // TEST 4
    @Test
    @Order(1)
    void testGetByCountry() {
	given().when().get("/employees/country/Austria").then().body("size()", equalTo(2))
		.body("id", hasItems(1, 2)).body("first_name", hasItems("Daniel", "Karl"))
		.body("last_name", hasItems("Johnsson", "Karlson"))
		.body("position", hasItems("Taxi Driver", "Developer"))
		.body("country", hasItem("Austria")).statusCode(Response.Status.OK.getStatusCode());
    }

    @Test
    @Order(1)
    void testGetByCountryKO() {
	given().when().get("/employees/country/Russia").then().body("size()", equalTo(0))
		.statusCode(Response.Status.OK.getStatusCode());
    }

    // TEST 5
    @Test
    @Order(2)
    void testCreate() {
	javax.json.JsonObject jsonObject = Json.createObjectBuilder().add("first_name", "Johnny")
		.add("last_name", "Johnsson").add("position", "IT").add("age", 30)
		.add("salary", 2000).add("country", "France").build();

	given().contentType(MediaType.APPLICATION_JSON).body(jsonObject.toString()).when()
		.post("/employees").then().statusCode(Response.Status.CREATED.getStatusCode());
    }

    // TEST 6
    @Test
    @Order(3)
    void testUpdateById() {
	javax.json.JsonObject jsonObject = Json.createObjectBuilder()
		.add("first_name", "UpdatedName").build();

	given().contentType(MediaType.APPLICATION_JSON).body(jsonObject.toString()).when()
		.put("/employees/2").then().body("id", equalTo(2))
		.body("first_name", equalTo("UpdatedName")).body("last_name", equalTo("Karlson"))
		.body("position", equalTo("Developer")).body("country", equalTo("Austria"))
		.statusCode(Response.Status.OK.getStatusCode());
    }

    @Test
    @Order(3)
    void testUpdateByIdKO() {
	javax.json.JsonObject jsonObject = Json.createObjectBuilder()
		.add("first_name", "UpdatedFirstName").build();
	given().contentType(MediaType.APPLICATION_JSON).body(jsonObject.toString()).when()
		.put("/employees/2000").then()
		.statusCode(Response.Status.NOT_FOUND.getStatusCode());
    }

    // TEST 7
    @Test
    @Order(4)
    void testDeleteEmployee() {
	given().when().delete("/employees/2").then()
		.statusCode(Response.Status.NO_CONTENT.getStatusCode());

	given().when().get("/employees/2").then()
		.statusCode(Response.Status.NOT_FOUND.getStatusCode());
    }

    @Test
    @Order(4)
    void testDeleteMovieKO() {
	given().when().get("/emoloyees/2000").then()
		.statusCode(Response.Status.NOT_FOUND.getStatusCode());
    }

}
