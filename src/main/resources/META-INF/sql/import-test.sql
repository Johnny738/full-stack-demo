INSERT INTO employee(id ,first_name , last_name, position, age, imageUrl, salary, country)
VALUES (1, 'Daniel','Johnsson','Taxi Driver', 23 ,'someImageUrl1',3000, 'Austria');

INSERT INTO employee(id ,first_name , last_name, position, age, imageUrl , salary, country)
VALUES (2, 'Karl','Karlson','Developer', 35, 'someImageUrl2' , 5000, 'Austria');

ALTER SEQUENCE hibernate_sequence RESTART WITH 3;
