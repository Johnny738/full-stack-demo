package org.gs;

import java.net.URI;
import java.util.List;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

@Path("/employees")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class EmployeeResource {

    @Inject
    EmployeeRepository employeeRepository;

    @GET
    public Response getAll() {
	List<Employee> employees = employeeRepository.getAllEmplSorted();
	return Response.ok(employees).build();
    }

    @GET
    @Path("{id}")
    public Response getById(@PathParam("id") Long id) {
	return employeeRepository.findByIdOptional(id).map(empl -> Response.ok(empl).build())
		.orElse(Response.status(Status.NOT_FOUND).build());
    }

    @GET
    @Path("first_name/{first_name}")
    public Response getByFirstName(@PathParam("first_name") String first_name) {
	return employeeRepository.find("first_name", first_name).singleResultOptional()
		.map(empl -> Response.ok(empl).build())
		.orElse(Response.status(Status.NOT_FOUND).build());
    }

    @GET
    @Path("country/{country}")
    public Response getByCoutry(@PathParam("country") String country) {
	List<Employee> movies = employeeRepository.findByCountry(country);

	return Response.ok(movies).build();

    }

    @POST
    @Transactional
    public Response createEmployee(Employee employee) {
	employeeRepository.persist(employee);
	if (employeeRepository.isPersistent(employee)) {

	    return Response.created(URI.create("/employees/" + employee.getId())).build();

	}
	return Response.status(Status.BAD_REQUEST).build();
    }

    @PUT
    @Path("{id}")
    @Transactional
    public Response updateById(@PathParam("id") Long id, Employee employee) {
	return employeeRepository.findByIdOptional(id).map(m -> {
	    m.setFirst_name(employee.getFirst_name());
	    return Response.ok(m).build();
	}).orElse(Response.status(Status.NOT_FOUND).build());
    }

    @DELETE
    @Path("{id}")
    @Transactional
    public Response deleteEmployee(@PathParam("id") Long id) {
	boolean deleteEmpl = employeeRepository.deleteById(id);
	if (deleteEmpl) {
	    return Response.noContent().build();
	}
	return Response.status(Status.NOT_FOUND).build();
    }

}
