package org.gs;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import io.quarkus.panache.common.Sort;

@ApplicationScoped
public class EmployeeRepository implements PanacheRepository<Employee> {

    public List<Employee> findByCountry(String country) {
	return list("SELECT e FROM Employee e WHERE e.country = ?1 ORDER BY id DESC", country);
    }

    public List<Employee> getAllEmplSorted() {
	Sort sort1 = Sort.by("id");
	return listAll(sort1);
    }

}
